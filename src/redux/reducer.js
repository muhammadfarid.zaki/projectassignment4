import {connect} from 'react-redux';
import index from '../screens/crudRedux';

const initialState = {
  name: 'Zak',
  students: [
    {id: 1, name: 'abi', address: 'jakarta'},
    {id: 2, name: 'data', address: 'redux'},
  ],
};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD-STUDENT':
      return {
        ...state,
        students: [...state.students, action.payload],
      };
    case 'DELETE-STUDENT':
      return {
        ...state,
        students: state.students.filter(value => {
          return value.id != action.payload;
        }),
      };
    case 'EDIT-STUDENT':
      return {
      ...state,
      students: action.payload
      };

    default:
      return state;
  }
};

export default reducer;
