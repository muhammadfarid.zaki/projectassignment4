import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CariFilm from '../screens/Cari Film';
import InformasiDetail from '../screens/Informasi Detail';
import LoginPage from '../screens/LoginPage';
import Statement from '../screens/Statement';
import Splash from '../screens/Splash';
import {Component} from 'react/cjs/react.production.min';
import Register from '../screens/Sign Up';
import ForgotPassword from '../screens/Forgot Password';
import crudRedux from '../screens/crudRedux';
import testLooping from '../screens/testLooping';
import messaging from '@react-native-firebase/messaging';
import navigation from "@react-navigation/native"
import inbox from "../screens/Inbox"
import inboxDetail from "../screens/inboxDetail"
const Stack = createNativeStackNavigator();

export default class App extends Component {
  componentDidMount() {
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification.body,
      );
      this.props.navigation.navigate('inbox');
    });
  }
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Login Page"
            component={LoginPage}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Sign Up"
            component={Register}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Forgot Password"
            component={ForgotPassword}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Cari Film"
            component={CariFilm}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Informasi Detail Film"
            component={InformasiDetail}
          />
          <Stack.Screen name="inbox" component={inbox} options={{headerShown: false}}/>
          <Stack.Screen name="inboxDetail" component={inboxDetail} options={{headerShown: false}} />
          <Stack.Screen name="Statement" component={Statement} />
          <Stack.Screen name="crudRedux" component={crudRedux} />
          <Stack.Screen name="testLooping" component={testLooping} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
