import React, {Component} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  ScrollView,
  Button,
} from 'react-native';
import CButton from '../../components/atoms/CButton';
import axios from 'axios';

export default class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: [],
      users: [],
      anastasia: [],
      testArray: [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
        20,
      ],
      hasilFibo: [],
    };
  }
  componentDidMount() {
    axios
      .get('https://jsonplaceholder.typicode.com/comments?postId=1')
      .then(res => this.setState({post: res.data}));

    axios
      .get('https://jsonplaceholder.typicode.com/users')
      .then(res => this.setState({users: res.data}));
  }
  test1(x, i) {
    return (
      <View key={i}>
        {x % 2 != 0 && <Text style={styles.text}>ganjil: {x}</Text>}
      </View>
    );
  }
  test2(x, i) {
    return (
      <View key={i}>
        {x % 2 == 0 && x != 0 && <Text style={styles.text}>genap: {x}</Text>}
      </View>
    );
  }
  test3(x, i) {
    if (x > 1) {
      for (let y = 2; y < x; y++) {
        if (x % y == 0) {
          return;
        }
      }
      return (
        <View key={i}>
          <Text style={styles.text}>prima: {x}</Text>
        </View>
      );
    }
  }
  test4() {
    let a = 0,
      b = 0,
      c = 1,
      result = a + b;
    for (let i = 0; i < 20; i++) {
      this.state.hasilFibo.push(result);

      a = b;
      b = c;
      c = result;
      result = b + c;
      if (result > 21) {
        break;
      }
    }
    this.setState({hasilFibo: this.state.hasilFibo});
  }
  testPost(x, i) {
    return (
      <View key={i} style={{flex: 1, borderWidth: 3, marginBottom: 10}}>
        <Text style={styles.text}>post id:{x.postId}</Text>
        <Text style={styles.text}>id:{x.id}</Text>
        <Text style={styles.text}>name:{x.name}</Text>
        <Text style={styles.text}>email:{x.email}</Text>
        <Text style={styles.text}>body:{x.body}</Text>
      </View>
    );
  }
  testUsers(x, i) {
    return (
      <View key={i} style={{flex: 1, marginBottom: 10, borderWidth: 3}}>
        <Text style={styles.text}>id:{x.id} </Text>
        <Text style={styles.text}>email:{x.email} </Text>
        <Text style={styles.text}>name:{x.name} </Text>
        <Text style={styles.text}>phone:{x.phone} </Text>
      </View>
    );
  }
  testAnas(x, i) {
    return (
      <View key={i} style={{flex: 1, marginBottom: 10, borderWidth: 3}}>
        <Text style={styles.text}>id:{x.id} </Text>
        <Text style={styles.text}>email:{x.email} </Text>
        <Text style={styles.text}>name:{x.name} </Text>
        <Text style={styles.text}>phone:{x.phone} </Text>
        <Text style={styles.text}>website:{x.website} </Text>
      </View>
    );
  }
  testAnastasia() {
    const t = this.state.users.filter(x => {
      return x.website == 'anastasia.net';
    });
    t.length > 0 && this.setState({anastasia: t});
  }
  render() {
    const {post, users, anastasia} = this.state;

    return (
      <ScrollView style={{backgroundColor: 'grey'}}>
        <View style={{flex: 1}}>
          <CButton
            title="click"
            onPress={() => {
              this.test4();
            }}
          />
          <CButton
            title="click"
            onPress={() => {
              this.testAnastasia();
            }}
          />
          {/* <View>{post && post.map(this.testPost)}</View> */}
          {/* <View style={{flex: 1}}>{users && users.map(this.testUsers)}</View> */}
          <View style={{flex: 1}}>
            {anastasia && anastasia.map(this.testAnas)}
          </View>
          <View></View>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          {/* <View>{this.state.testArray.map(this.test1)}</View> */}
          {/* <View>{this.state.testArray.map(this.test2)}</View> */}
          {/* <View>{this.state.testArray.map(this.test3)}</View> */}
        </View>

        {this.state.hasilFibo &&
          this.state.hasilFibo.map((x, i) => {
            return (
              <View key={i}>
                <Text style={styles.text}>Fibonacci: {x}</Text>
              </View>
            );
          })}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  text: {color: 'black', fontSize: 20, fontWeight: 'bold'},
});
