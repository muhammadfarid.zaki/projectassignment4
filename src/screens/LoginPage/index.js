import axios from 'axios';
import React, {Component} from 'react';
import {
  Linking,
  View,
  Text,
  TextInput,
  StyleSheet,
  ScrollView,
  Button,
  TouchableOpacity,
  Alert,
  ImageBackground,
  Image,
} from 'react-native';
import CButton from '../../components/atoms/CButton';
import Facebook from 'react-native-vector-icons/FontAwesome5Pro';
import Google from 'react-native-vector-icons/FontAwesome5Pro';
import Twitter from 'react-native-vector-icons/AntDesign';
import Apple from 'react-native-vector-icons/FontAwesome5Pro';
import User from 'react-native-vector-icons/FontAwesome';
import Key from 'react-native-vector-icons/Entypo';
import EyeIcon from 'react-native-vector-icons/FontAwesome';
import firestore from '@react-native-firebase/firestore';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      dataUsers: [],
      dataReg: {},
      iconname: 'eye-slash',
      showpass: true,
      dataFire: [],
    };
  }
  componentDidMount() {
    const {route} = this.props;

    axios
      .get('https://dummyjson.com/users')
      .then(res => this.setState({dataUsers: res.data.users}))

      .catch(error => console.log(error, 'error'));

    firestore()
      .collection('Users')
      .doc('Z4Wcf6C7fOJUs6bYwqbd')
      .onSnapshot(x => this.setState({dataFire: x.data()}));

    route.params &&
      this.setState({
        dataReg: {
          username: route.params.username,
          password: route.params.password,
        },
      });
      
  }
  _HideShow() {
    this.setState(x => ({
      iconname: x.iconname == 'eye' ? 'eye-slash' : 'eye',
      showpass: !x.showpass,
    }));
  }
 
  _conditionalButton() {
    
    
      this.state.dataReg && this.state.dataUsers.push(this.state.dataReg);
    
    const x = this.state.dataUsers.filter(x => {
      return (
        x.username == this.state.username && x.password == this.state.password
      );
    });
    x.length > 0
      ? this.props.navigation.navigate('Cari Film')
      : Alert.alert('Username or Password is incorrect');
  }

  render() {
    
    return (
      <View style={{flex: 1}}>
        <ImageBackground
          blurRadius={3}
          resizeMode="cover"
          style={{flex: 1}}
          source={{
            uri: 'https://cdn.kibrispdr.org/data/minimalist-phone-wallpaper-2.jpg',
          }}>
          <ScrollView>
            <View
              style={{
                flex: 1 / 3,
                justifyContent: 'flex-end',
              }}>
              <View
                style={{alignItems: 'center', marginTop: 15, marginBottom: 28}}>
                <Image source={{uri:'https://cdn4.iconfinder.com/data/icons/logos-3/279/phonegap-512.png'}} style={gaya.logo}/>
                {/* <Text
                  style={{
                    
                    fontWeight: 'bold',
                    color: 'black',
                    fontSize: 30,
                  }}>
                  Cocoanut Media
                </Text> */}
              </View>
            </View>

            <View style={gaya.pembungkus}>
              <Text
                style={{
                  marginTop: 15,
                  marginLeft: 35,
                  fontSize: 30,
                  fontWeight: 'bold',
                  color: 'white',
                }}>
                Sign In
              </Text>
              <View style={{alignItems: 'center', marginTop: 20}}>
                <View style={{justifyContent: 'center'}}>
                  <TextInput
                    style={gaya.Kotak}
                    placeholder="Cocoanut ID.."
                    placeholderTextColor={'black'}
                    onChangeText={value => this.setState({username: value})}
                  />
                  <User
                    color={'black'}
                    style={{left: 10, position: 'absolute'}}
                    size={25}
                    name="user-circle"
                  />
                </View>
                <View style={{justifyContent: 'center'}}>
                  <TextInput
                    style={{...gaya.Kotak, marginTop: 18}}
                    placeholder="Password.."
                    placeholderTextColor={'black'}
                    secureTextEntry={this.state.showpass}
                    onChangeText={value => this.setState({password: value})}
                  />
                  <Key
                    color={'black'}
                    style={{left: 10, position: 'absolute'}}
                    size={25}
                    name="key"
                  />
                  <EyeIcon
                    color={'black'}
                    style={{right: 10, position: 'absolute'}}
                    size={25}
                    name={this.state.iconname}
                    onPress={() => this._HideShow()}
                  />
                </View>
              </View>

              <View style={{alignItems: 'center', marginTop: 15}}>
                <CButton
                  title="Login"
                  onPress={() => {
                    this._conditionalButton();
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 35,
                  marginLeft: 35,
                  marginBottom: 20,
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                    marginLeft: 0,
                    color: 'cyan',
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Forgot Password');
                  }}>
                  Forgot your password?
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                    marginLeft: 25,
                    color: 'white',
                  }}>
                  or
                </Text>
                <Text
                  style={{
                    fontWeight: 'bold',
                    fontSize: 15,
                    marginLeft: 25,
                    color: 'cyan',
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Sign Up');
                  }}>
                  Sign Up
                </Text>
              </View>
            </View>
            <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginTop: 10,
                  marginBottom:10
                  
                }}>
                <Facebook
                  style={{marginHorizontal: 20}}
                  color="#3b5998"
                  size={45}
                  name="facebook"
                  onPress={() => Linking.openURL('https://facebook.com/login')}
                />
                <Google
                  style={{marginHorizontal: 20}}
                  color="#db4a39"
                  size={45}
                  name="google-plus"
                  onPress={() =>
                    Linking.openURL(
                      'https://accounts.google.com/signin/v2/identifier?sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin',
                    )
                  }
                />
                <Twitter
                  style={{marginHorizontal: 20}}
                  color="#00ACEE"
                  size={45}
                  name="twitter"
                  onPress={() =>
                    Linking.openURL('https://mobile.twitter.com/login')
                  }
                />
                <Apple
                  style={{marginHorizontal: 20}}
                  color="white"
                  size={45}
                  name="apple"
                  onPress={() => Linking.openURL('https://www.icloud.com/')}
                />
              </View>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const gaya = StyleSheet.create({
  logo: {
    width: 150,
    height: 150,
  },
  pembungkus: {
    backgroundColor: 'black',
    flex: 2 / 3,
    justifyContent: 'space-evenly',
    paddingBottom: 15,

    borderWidth: 1,
    borderColor: 'black',
    marginHorizontal: 25,
    borderRadius: 10,
    opacity: 0.85,
    marginBottom: 0,
  },
  Kotak: {
    fontSize: 20,
    flexDirection: 'row',
    fontWeight: 'bold',
    width: 300,
    height: 60,
    paddingLeft: 40,
    padding: 10,
    backgroundColor: 'grey',
    marginVertical: 10,
    borderRadius: 15,
    borderColor: 'black',
    borderWidth: 1,
  },
});
