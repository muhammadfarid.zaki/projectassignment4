import React, {Component} from 'react';
import {View, Text, TextInput, ImageBackground, ScrollView} from 'react-native';
import CButton from '../../components/atoms/CButton';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeleteUser from 'react-native-vector-icons/AntDesign';
import {SafeAreaView} from 'react-native-safe-area-context';
import {connect} from 'react-redux';

export class index extends Component {
  constructor() {
    super();
    this.state = {id:'',
      name: '',
      address: '',
      data: {id:'',
        name: '',
        address: '',
      },
      table: [],
      tablebaru: [],
    };
  }
  componentDidMount() {}
  triggerButton() {
    const {students} = this.props;
    const {table, name, address} = this.state;
    const id = students.length + 1;
    const data = {name, address, id};
   
    this.props.add(data);
    this.setState({name: '', address: ''});
    //   data: {name: this.state.name, address: this.state.address},
    //   students: [
    //     ...students,
    //     {
    //       id,
    //       name,
    //       address,
    //     },
    //   ],
    //   name: '',
    //   address: '',
    // });
  }
  deleteUser(etc) {
    const {students} = this.props;
    const {table, name, id, address} = this.state;
    this.props.delete(etc);
  }
  editUnique(x){
    this.setState({id:x.id,name:x.name,address:x.address})
  }
  
  editName() {
    const {students} = this.props;
    const { name, address,id} = this.state;
    const data = {id, name, address};
    
    const uniqueStudent = students.map(x=>{
      if (x.id===data.id){
        (x.id= data.id),
        (x.name=data.name),
        (x.address=data.address);
        return x;
      }else{
        return x
      }
    })
    
    this.props.edit(uniqueStudent)
    this.setState({
      name:'',address:''
    })
  }

  render() {
    const {id, table, name, address} = this.state;

    const {students} = this.props;
    

    return (
      <View style={{flex: 1}}>
        <SafeAreaView>
          <ScrollView>
            {/* <ImageBackground
              source={{uri: 'https://www.mahagramin.in/images/background.jpg'}}>
              <View
                style={{
                  height: 250,
                }}>
                <View
                  style={{
                    flex: 3,
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}>
                  <View>
                    <Text style={{color: 'white'}}>Name:</Text>
                    <Text style={{color: 'white'}}>{data.name}</Text>
                  </View>
                  <View>
                    <Text style={{color: 'white'}}>Address:</Text>
                    <Text style={{color: 'white'}}>{data.address}</Text>
                  </View>
                </View>
                <View style={{flex: 1, alignItems: 'center'}}>
                  <Text style={{color: 'white'}}>
                    {randomCard < 1000 && randomCard
                      ? randomCard + 1000
                      : randomCard}{' '}
                    {randomCard2 < 1000 && randomCard2
                      ? randomCard2 + 1000
                      : randomCard2}{' '}
                    {randomCard3 < 1000 && randomCard3
                      ? randomCard3 + 1000
                      : randomCard3}{' '}
                    {randomCard4 < 1000 && randomCard4
                      ? randomCard4 + 1000
                      : randomCard4}
                  </Text>
                </View>
              </View>
            </ImageBackground> */}

            <TextInput
              placeholder="input something"
              value={name}
              onChangeText={value => this.setState({name: value})}
              style={{borderWidth: 1}}
            />
            <TextInput
              placeholder="input something"
              value={address}
              onChangeText={value => this.setState({address: value})}
              style={{borderWidth: 1}}
            />
            <View style={{flexDirection: 'row',justifyContent:'space-around'}}>
              <CButton
                title="Add"
                style={{backgroundColor: 'yellow',width:'49%'}}
                onPress={() => {
                  this.triggerButton();
                }}
              />
              <CButton title="Edit" style={{backgroundColor: 'red',width:'49%'}} onPress={()=>{this.editName()}}/>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}>
              <View
                style={{
                  flex: 2 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text style={{}}>
                  <AntDesign style={{}} size={20} name="table" />
                  ID
                </Text>
              </View>
              <View
                style={{
                  flex: 8 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>Name</Text>
              </View>
              <View
                style={{
                  flex: 8 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>Address</Text>
              </View>
              <View
                style={{
                  flex: 2 / 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 1,
                }}>
                <Text>X</Text>
              </View>
            </View>
            {students &&
              students.map((v, i) => {
                return (
                  <View
                    key={i}
                    style={{
                      flexDirection: 'row',
                    }}>
                    <View
                      style={{
                        flex: 2 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text
                        onPress={() => {
                          this.editUnique(v);
                        }}
                        >
                        {v.id}
                      </Text>
                    </View>
                    <View
                      style={{
                        flex: 8 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text>{v.name}</Text>
                    </View>
                    <View
                      style={{
                        flex: 8 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <Text>{v.address}</Text>
                    </View>
                    <View
                      style={{
                        flex: 2 / 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderWidth: 1,
                      }}>
                      <DeleteUser
                        name="deleteuser"
                        onPress={() => {
                          this.deleteUser(v.id);
                        }}
                      />
                    </View>
                  </View>
                );
              })}
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    students: state.students,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    add: data => {
      dispatch({
        type: 'ADD-STUDENT',
        payload: data,
      });
    },
    delete: data => {
      dispatch({
        type: 'DELETE-STUDENT',
        payload: data,
      });
    },
    edit:value=>{
        dispatch({
            type:"EDIT-STUDENT",
            payload:value
        })
    }
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(index);
