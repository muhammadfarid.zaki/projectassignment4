import React, {Component} from 'react';
import {View, Text, TextInput, ImageBackground, ScrollView} from 'react-native';
import CButton from '../../components/atoms/CButton';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeleteUser from 'react-native-vector-icons/AntDesign';
import {SafeAreaView} from 'react-native-safe-area-context';
import AsyncStorageLib from '@react-native-async-storage/async-storage';
import messaging from '@react-native-firebase/messaging';
import ArrowRight from 'react-native-vector-icons/AntDesign';
import Trash from 'react-native-vector-icons/Octicons'
import Dot from 'react-native-vector-icons/Entypo'
export default class index extends Component {
  constructor() {
    super();
    this.state = {
      dataInbox: [],
      hasRead:''
    };
  }
 
  componentDidMount() {
    this._getData();
    messaging().getToken();
    // .then(x=>console.log(x))
    
  }
  _dataRead=async()=>{
    
  }
  _getData = async () => {
    try {
      const jsonValue = await AsyncStorageLib.getItem('newInboxData');
      return jsonValue != null
        ? this.setState({dataInbox: JSON.parse(jsonValue)})
        : null;
        
    } catch (e) {
      // error reading value
    }
  };
  _deleteData = async() => {
    await AsyncStorageLib.clear();
    this.setState({dataInbox:[]})
}

  render() {
    const {dataInbox} = this.state;
console.log(dataInbox)
    return (
      <View style={{flex:1,backgroundColor: 'brown'}}>
      
        <View
          style={{
            height: 60,
            backgroundColor: 'black',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: 'white', fontSize: 25, fontWeight: 'bold'}}
          onPress={()=>{this._getData()}}>
            Inbox
          </Text>
          <Trash onPress={()=>{this._deleteData()}} style={{right:20,position:'absolute'}} size={25} name="trashcan" color={"white"}/>
        </View>
        <ScrollView >
        {dataInbox &&
          dataInbox.map((v, i) => {
            return (
              <View key={i}>
                <View
                  style={{
                    backgroundColor: v.isRead==0?'darkslategrey':'grey',
                    padding: 10,
                    marginHorizontal: 20,
                    marginTop: 20,
                    borderWidth: 5,
                    borderRadius: 25,
                  }}>
                  <View
                    style={{
                      flex: 10 / 20,
                      backgroundColor: 'dimgrey',
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: 5,
                      borderRadius: 10,
                    }}>
                    <Text
                      style={{
                        color: 'white',
                        fontSize: 25,
                        fontWeight: 'bold',
                      }}>
                      {v.title}
                    </Text>{v.isRead==0&&<Dot style={{ right:0,position:'absolute'}} color={'darkslategrey'} size={60} name="dot-single"/>}
                  </View>
                  <View
                    style={{
                      paddingHorizontal: 10,
                      height: 70,
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderWidth: 5,
                      marginTop: 10,
                      borderRadius: 5,
                    }}>
                    <Text style={{color: 'white', fontSize: 22}}>{v.body}</Text>
                  </View>
                  <View style={{alignItems: 'flex-end'}}>
                    {v.time&&<Text style={{left:10,position:"absolute",color:'white',marginTop:5}}>{v.time}</Text>}
                    <Text
                      style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}
                      onPress={() => {
                        if (v.image != null) {
                          return this.props.navigation.navigate('inboxDetail', {
                            message: {
                              title: v.title,
                              body: v.body,
                              image: v.image,
                              time:v.time,
                              id:v.id
                            },
                          });
                        } else {
                          this.props.navigation.navigate('inboxDetail', {
                            message: {title: v.title, body: v.body,time: v.time,id:v.id},
                          });
                        };
                      }}>
                      •••
                    </Text>
                  </View>
                </View>
              </View>
            );
          })}
      </ScrollView>
      </View>
    );
  }
}
