import AsyncStorageLib from '@react-native-async-storage/async-storage';
import React, {Component} from 'react';
import {
  ScrollView,
  Image,
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Button,
} from 'react-native';
import ArrowLeft from 'react-native-vector-icons/AntDesign';
import Trash from "react-native-vector-icons/Octicons"
export default class inboxDetailApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      daMessage: [],
    };
  }
  componentDidMount() {}
  _isReadData = async(id) => {
    try{
        let inboxJSON= await AsyncStorageLib.getItem('newInboxData');
        let inboxArray = JSON.parse(inboxJSON);
        filteredInbox = inboxArray.map(e=>{
            if(e.id==id){e.isRead="1"}return e

        })
        AsyncStorageLib.setItem('newInboxData', JSON.stringify(filteredInbox));
      this.props.navigation.navigate('inbox')  
        
    }
    catch(error){
        console.log(error)
    }
};

  _removeData = async(id) => {
    try{
        let inboxJSON= await AsyncStorageLib.getItem('newInboxData');
        let inboxArray = JSON.parse(inboxJSON);
        filteredInbox = inboxArray.filter(e=>{
            return e.id !== id

        })
        AsyncStorageLib.setItem('newInboxData', JSON.stringify(filteredInbox));
      this.props.navigation.navigate('inbox')  
        
    }
    catch(error){
        console.log(error)
    }
};
  render() {
    const {message} = this.props.route.params;
    return (<View style={{flex: 1,backgroundColor:'brown'}}>
      
        <View
          style={{
            height: 60,
            backgroundColor: 'black',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ArrowLeft
            style={{left: 10, position: 'absolute'}}
            color={'white'}
            size={25}
            name="arrowleft"
            onPress={() => {
              this._isReadData(message.id);
            }}
          />
          <Text style={{color: 'white', fontSize: 25, fontWeight: 'bold'}}>
            Detail..
          </Text>
          <Trash onPress={()=>{this._removeData(message.id)}} style={{right:20,position:'absolute'}} size={25} name="trashcan" color={"white"}/>
        </View>
        <ScrollView>
        <View style={{flex: 1}}>
          <View
            style={{
              borderWidth: 5,
              borderColor: 'black',
              backgroundColor: 'dimgrey',
              margin: 25,
              borderRadius: 25,
            }}>
            <View
              style={{
                borderRadius: 10,
                margin: 20,

                backgroundColor: 'black',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{color: 'white', fontSize: 30, fontWeight: 'bold'}}>
                {message.title}
              </Text>
            </View>
            {this.props.children}
            <View style={{marginHorizontal: 20, marginBottom: 10}}>
              <View
                style={{
                  paddingHorizontal: 7,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderWidth: 5,
                  borderRadius: 10,
                  backgroundColor: 'grey',
                }}>
                <Text
                  style={{fontWeight: 'bold', color: 'white', fontSize: 25}}>
                  {message.body}
                </Text>
              </View>
              <View style={{alignItems: 'flex-end'}}>
                <Text style={{color: 'white', marginTop: 5}}>
                  {message.time}
                </Text>
              </View>
            </View>
          </View>
          
        </View>
        {message.image && (
          <View
            style={{
              flex:1,
              justifyContent:'center',
              alignItems: 'center',
              borderColor: 'black',
              borderRadius: 25,marginBottom:25,
              borderWidth: 5,
              marginHorizontal: 25,
              backgroundColor: 'dimgrey',
              paddingBottom:25
            }}>
            <View
              style={{
                borderRadius: 10,
                
                margin: 20,
                backgroundColor: 'black',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{marginHorizontal:47, color: 'white', fontSize: 30, fontWeight: 'bold'}}>
                Image Recieved
              </Text>
            </View>
           <View>
              <Image
                style={{
                  borderWidth: 5,
                  borderColor: 'black',
                  borderRadius: 10,
                  height: 150,
                  width: 150,
                }}
                source={{uri: `${message.image}`}}
              /></View>
           
          </View> )}
      </ScrollView></View>
    );
  }
}

const styles = StyleSheet.create({
  text: {color: 'black', fontSize: 20, fontWeight: 'bold'},
});
