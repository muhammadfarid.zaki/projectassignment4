import React, {useEffect, Component} from 'react';
import {Alert} from 'react-native';
import {Provider} from 'react-redux';
import Router from './src/navigation';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/redux/store';
import messaging from '@react-native-firebase/messaging';
import AsyncStorageLib from '@react-native-async-storage/async-storage';

// foreground
messaging().onMessage(async remoteMessage => {
  setInboxData(remoteMessage);
  Alert.alert("New Notification",
    "Open notification inbox"
  )
});

// background
messaging().setBackgroundMessageHandler(async remoteMessage => {
  setInboxData(remoteMessage)
});
const time = new Date().toLocaleString('en-GB', { timeZone: 'Asia/Jakarta' })

const setInboxData = async (remoteMessage) => {
  try {
    let getInboxData = await AsyncStorageLib.getItem('newInboxData');
    getInboxData = JSON.parse(getInboxData);
    
    

    if (!getInboxData) {
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(
          [
            {
              title: remoteMessage.notification.title,
              body: remoteMessage.notification.body,
              image: remoteMessage.notification.android.imageUrl,
              time:time,
              data: remoteMessage.data,
              isRead:0,
              id:Math.round(Math.random()*1000000)
              
            }
          ]
        )
      )
    } else {
      const arrayData = [
        ...[
          {
            title: remoteMessage.notification.title,
            body: remoteMessage.notification.body,
            image: remoteMessage.notification.android.imageUrl,
            time:time,
            data: remoteMessage.data,
            isRead:0,
            id:Math.floor(Math.random()*1000000)
          }
        ],
        ...getInboxData
      ].slice(0, 20)
      AsyncStorageLib.setItem('newInboxData',
        JSON.stringify(arrayData)
      )
    }
  } catch (error) {
    console.log(error);
  }
}
  const App =()=>{
    return (
      <Provider store={Store}>
        <PersistGate loading={null} persistor={Persistor}>
          <Router />
        </PersistGate>
      </Provider>
    );
  }
export default App;
